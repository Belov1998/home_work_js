const films = [
	{
		title: 'The Green Mile',
		creationYear: 1999,
		country: ['USA'],
		budget: '$60 000 000',
		actors: [
			{
				name: 'Tom Hanks',
				age: 63,
				role: 'Paul Edgecomb',
			},
			{
				name: 'David Morse',
				age: 65,
				role: 'Brutus Howell',
			},
			{
				name: 'Michael Clarke Duncan',
				age: 54,
				role: 'John Coffey',
			},
		],
		director: {
			name: 'Frank Darabont',
			age: 60,
			oscarsCount: 0,
		}
	},
	{
		title: 'Inception',
		creationYear: 2010,
		country: ['USA'],
		budget: '$160 000 000',
		actors: [
			{
				name: 'Leonardo DiCaprio',
				age: 44,
				role: 'Cobb',
			},
			{
				name: 'Joseph Gordon-Levitt',
				age: 38,
				role: 'Arthur',
			},
			{
				name: 'Ellen Page',
				age: 32,
				role: 'Ariadne',
			},
			{
				name: 'Tom Hardy',
				age: 41,
				role: 'Eames',
			},
		],
		director: {
			name: 'Christopher Nolan',
			age: 48,
			oscarsCount: 0,
		}
	},
	{
		title: 'Forrest Gump',
		creationYear: 1994,
		country: ['USA'],
		budget: '$55 000 000',
		actors: [
			{
				name: 'Tom Hanks',
				age: 63,
				role: 'Forrest Gump',
			},
			{
				name: 'Robin Wright',
				age: 53,
				role: 'Jenny Curran',
			},
			{
				name: 'Sally Field',
				age: 72,
				role: 'Mrs. Gump',
			},
		],
		director: {
			name: 'Robert Zemeckis',
			age: 68,
			oscarsCount: 1,
		}
	},
	{
		title: 'Interstellar',
		creationYear: 2014,
		budget: '$165 000 000',
		country: ['USA', 'Great Britain', 'Canada'],
		actors: [
			{
				name: 'Matthew McConaughey',
				age: 49,
				role: 'Cooper',
			},
			{
				name: 'Anne Hathaway',
				age: 36,
				role: 'Brand',
			},
			{
				name: 'Jessica Chastain',
				age: 42,
				role: 'Murph',
			},
			{
				name: 'Michael Caine',
				age: 86,
				role: 'Professor Brand',
			},
			{
				name: 'Matt Damon',
				age: 48,
				role: 'Mann',
			},
		],
		director: {
			name: 'Christopher Nolan',
			age: 48,
			oscarsCount: 0,
		}
	},
	{
		title: 'Catch Me If You Can',
		creationYear: 2002,
		budget: '$52 000 000',
		country: ['USA', 'Canada'],
		actors: [
			{
				name: 'Tom Hanks',
				age: 63,
				role: 'Carl Hanratty',
			},
			{
				name: 'Leonardo DiCaprio',
				age: 36,
				role: 'Frank Abagnale Jr.',
			},
			{
				name: 'Christopher Walken',
				age: 76,
				role: 'Frank Abagnale',
			},
			{
				name: 'Amy Adams',
				age: 44,
				role: 'Brenda Strong',
			},
		],
		director: {
			name: 'Steven Spielberg',
			age: 72,
			oscarsCount: 4,
		}
	},
];
//РµСЃС‚СЊ Р»Рё Р°РєС‚РµСЂ РІ С„РёР»СЊРјРµ
function searchActor(actors, name) {
	let flag = false;
	actors.forEach(item=>{
		if (item.name === name) flag = true;
	});
	return flag;
}

// СЃРѕСЃС‚Р°РІР»СЏРµРј СЃРїРёСЃРѕРє С‡С‚РѕР±С‹ Р°РєС‚РµСЂС‹  РЅРµ РїРѕРІС‚РѕСЂСЏР»РёСЃСЊ
function sumActor(films) {
	let sum = [];
	films.forEach(film => {
		let actors = film.actors;
		if (actors){
			actors.forEach(actor => {
				const {name, age} = actor;
				const newActor = {name: name, age: age};
				if (!sum.find((obj) => {return obj.name == newActor.name})){
					sum.push(newActor);
				}
			});
		}
	});
	return sum;
}

// 1
function searchAge(films) {
	let filmsNoOscars = films.filter(item => item.director.oscarsCount === 0);
	let actors =sumActor(filmsNoOscars);
	let sumAge=0;
	actors.forEach(item=>{
		sumAge+=item.age;
	});

	let result= Math.floor(sumAge/actors.length);
	alert(result);
}

// 2
function nameActore(films) {
	let tempFilms = films.filter(item => item.creationYear > 1975 && searchActor(item.actors,'Tom Hanks'));
	let actors = sumActor(tempFilms);
	let result = [];
	actors.forEach(actor => {
		if (actor.name != 'Tom Hanks') result.push(actor.name);
	});
	alert(result);
}

// 3
function sumFilms(films) {
	let tempFilms = films.filter(item => item.director.age < 70 && !searchActor(item.actors,'Tom Hanks'));
	let resultSum =0;
	tempFilms.forEach(film =>{
		let budget =Number(film.budget.replace(/[$\s]/g,''));
		resultSum += budget;
	});
	alert(resultSum);
}

searchAge(films);
nameActore(films);
sumFilms(films);
